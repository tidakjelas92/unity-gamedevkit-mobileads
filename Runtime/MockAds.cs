using System;
using UnityEngine;

namespace UMNP.GamedevKit.MobileAds
{
    public class MockAds : MonoBehaviour, IMobileAdsProvider
    {
        [SerializeField] private MockBanner _banner;
        [SerializeField] private MockInterstitial _interstitial;
        [SerializeField] private MockRewarded _rewarded;

        private MockBanner _bannerInstance;
        private MockInterstitial _interstitialInstance;
        private MockRewarded _rewardedInstance;
        private Transform _transform;
        private bool _isAdsRemoved;
        private bool _isInitialized;
        private Action _pause;
        private Action _resume;

        public float BannerHeight
        {
            get
            {
                if (!_bannerInstance) return 0.0f;

                var actualSize = Banner.GetActualSize(_bannerInstance.Size);
                return actualSize.y;
            }
        }
        public bool IsAdsRemoved => _isAdsRemoved;
        public bool IsBannerActive => _bannerInstance ? _bannerInstance.isActiveAndEnabled : false;
        public bool IsInitialized => _isInitialized;
        public bool IsInterstitialLoaded => _interstitialInstance;
        public bool IsRewardedLoaded => _rewardedInstance;

        private void Awake()
        {
            _transform = transform;
            DontDestroyOnLoad(this);
        }

        public void CleanBanner()
        {
            if (!_bannerInstance) return;
            Destroy(_bannerInstance.gameObject);
            UI.SafeArea.ClearOffsets();
        }

        public void CleanInterstitial()
        {
            if (!_interstitialInstance) return;
            Destroy(_interstitialInstance.gameObject);
        }

        public void CleanRewarded()
        {
            if (!_rewardedInstance) return;
            Destroy(_rewardedInstance.gameObject);
        }

        public void HideBanner()
        {
            _bannerInstance.gameObject.SetActive(false);
            UI.SafeArea.ClearOffsets();
        }

        public void Initialize(Action pause, Action resume)
        {
            _pause = pause;
            _resume = resume;
            _isInitialized = true;
        }

        public void RequestBanner(Banner.Size size, Banner.Position position)
        {
            CleanBanner();
            if (IsAdsRemoved) return;

            _bannerInstance = Instantiate<MockBanner>(_banner, _transform);
            _bannerInstance.Position = position;
            _bannerInstance.Size = size;
            _bannerInstance.gameObject.SetActive(false);
        }

        public void RequestInterstitial()
        {
            CleanInterstitial();
            if (IsAdsRemoved) return;

            _interstitialInstance = Instantiate<MockInterstitial>(_interstitial, _transform);
            _interstitialInstance.gameObject.SetActive(false);
        }

        public void RequestRewarded()
        {
            CleanRewarded();
            _rewardedInstance = Instantiate<MockRewarded>(_rewarded, _transform);
            _rewardedInstance.gameObject.SetActive(false);
        }

        public void ShowBanner(Banner.Position position)
        {
            if (!_bannerInstance) return;
            _bannerInstance.Position = position;
            _bannerInstance.gameObject.SetActive(true);
            AdjustSafeArea();
        }

        public void ShowInterstitial(Action onClosed)
        {
            if (!IsInterstitialLoaded) return;

            _interstitialInstance.SetOnClosed(
                () =>
                {
                    onClosed?.Invoke();
                    _resume?.Invoke();
                }
            );
            _pause?.Invoke();
            _interstitialInstance.gameObject.SetActive(true);
        }

        public void ShowRewarded(Action onEarned, Action onSkipped)
        {
            if (!IsRewardedLoaded) return;

            _rewardedInstance.SetCallbacks(
                () =>
                {
                    onEarned?.Invoke();
                    _resume?.Invoke();
                },
                () =>
                {
                    onSkipped?.Invoke();
                    _resume?.Invoke();
                }
            );
            _rewardedInstance.gameObject.SetActive(true);
            _pause?.Invoke();
        }

        public void RemoveAds()
        {
            _isAdsRemoved = true;
            CleanBanner();
            CleanInterstitial();
            UI.SafeArea.ClearOffsets();
        }

        private void AdjustSafeArea()
        {
            switch (_bannerInstance.Position)
            {
                case Banner.Position.Top:
                    UI.SafeArea.Offset.top = Mathf.CeilToInt(BannerHeight);
                    break;
                case Banner.Position.Bottom:
                    UI.SafeArea.Offset.bottom = Mathf.CeilToInt(BannerHeight);
                    break;
                default:
                    break;
            }

            UI.SafeArea.FitAll();
        }
    }
}
