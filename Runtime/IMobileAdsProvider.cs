using System;

namespace UMNP.GamedevKit.MobileAds
{
    public interface IMobileAdsProvider
    {
        float BannerHeight { get; }
        bool IsAdsRemoved { get; }
        bool IsBannerActive { get; }
        bool IsInitialized { get; }
        bool IsInterstitialLoaded { get; }
        bool IsRewardedLoaded { get; }

        void CleanBanner();
        void HideBanner();
        void RequestBanner(Banner.Size size, Banner.Position position);
        void ShowBanner(Banner.Position position);

        void CleanInterstitial();
        void ShowInterstitial(Action onClosed);
        void RequestInterstitial();

        void CleanRewarded();
        void RequestRewarded();
        void ShowRewarded(Action onEarned, Action onSkipped);

        void RemoveAds();
    }
}
