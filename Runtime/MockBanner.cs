using TMPro;
using UnityEngine;

namespace UMNP.GamedevKit.MobileAds
{
    // Incomplete!!
    public class MockBanner : MonoBehaviour
    {
        [SerializeField] private RectTransform _bannerRect;
        [SerializeField] private TextMeshProUGUI _bannerResolutionText;

        private Banner.Size _size;
        private Banner.Position _position;

        public Banner.Size Size
        {
            get => _size;
            set
            {
                _size = value;
                var actualSize = Banner.GetActualSize(value);
                _bannerRect.sizeDelta = actualSize;
                _bannerResolutionText.SetText($"({actualSize.x}px, {actualSize.y}px) in {Screen.dpi}dpi\nACTUAL SIZE MAY VARY");
            }
        }

        public Banner.Position Position
        {
            get => _position;
            set
            {
                _position = value;
                switch (value)
                {
                    case Banner.Position.Top:
                        _bannerRect.anchorMin = new Vector2(0.5f, 1.0f);
                        _bannerRect.anchorMax = new Vector2(0.5f, 1.0f);
                        _bannerRect.pivot = new Vector2(0.5f, 1.0f);
                        _bannerRect.anchoredPosition = Vector2.zero;
                        break;
                    case Banner.Position.Bottom:
                        _bannerRect.anchorMin = new Vector2(0.5f, 0.0f);
                        _bannerRect.anchorMax = new Vector2(0.5f, 0.0f);
                        _bannerRect.pivot = new Vector2(0.5f, 0.0f);
                        _bannerRect.anchoredPosition = Vector2.zero;
                        break;
                }
            }
        }
    }
}
