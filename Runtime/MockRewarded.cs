using System;
using TMPro;
using UnityEngine;

namespace UMNP.GamedevKit.MobileAds
{
    public class MockRewarded : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _timeLeftText;
        [SerializeField] private GameObject _rewardedTextObject;
        [SerializeField] private GameObject _skipConfirmationObject;
        [SerializeField] private float _rewardTime;

        private Action _onEarned;
        private Action _onSkip;
        private float _timeElapsed;

        private bool RewardEarned => _timeElapsed > _rewardTime;

        private void Start()
        {
            _rewardedTextObject.SetActive(false);
            _skipConfirmationObject.SetActive(false);
        }

        private void Update()
        {
            if (RewardEarned) return;
            if (_skipConfirmationObject.activeInHierarchy) return;

            _timeElapsed += Time.unscaledDeltaTime;
            _timeLeftText.SetText(Mathf.CeilToInt(_rewardTime - _timeElapsed).ToString());

            if (RewardEarned)
                _rewardedTextObject.SetActive(true);
        }

        public void SetCallbacks(Action onEarned, Action onSkip)
        {
            _onEarned = onEarned;
            _onSkip = onSkip;
        }

        public void OnCloseButtonClicked()
        {
            if (RewardEarned)
            {
                _onEarned?.Invoke();
                Destroy(gameObject);
                return;
            }

            _skipConfirmationObject.SetActive(true);
        }

        public void OnSkipButtonClicked()
        {
            _onSkip?.Invoke();
            Destroy(gameObject);
        }

        public void OnCancelSkipButtonClicked() =>
            _skipConfirmationObject.SetActive(false);
    }
}
