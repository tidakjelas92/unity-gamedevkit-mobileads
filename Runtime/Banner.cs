using UnityEngine;

namespace UMNP.GamedevKit.MobileAds
{
    public static class Banner
    {
        public static Vector2 GetActualSize(in Size size)
        {
#if UNITY_EDITOR
            if (Screen.dpi == 0)
                Debug.LogWarning("Screen dpi is 0, try using simulator");
#endif
            var bannerSize = GetDPSize(size);
            return new Vector2(Utils.Units.ToPixel(bannerSize.x), Utils.Units.ToPixel(bannerSize.y));
        }

        /// <returns>
        /// size in dp or dip (density-independent pixels)
        /// </returns>
        public static Vector2 GetDPSize(in Size size)
        {
            switch (size)
            {
                case Size.Standard:
                    return new Vector2(320.0f, 50.0f);
                case Size.Large:
                    return new Vector2(320.0f, 100.0f);
                case Size.MediumRectangle:
                    return new Vector2(300.0f, 250.0f);
                case Size.Full:
                    return new Vector2(468.0f, 60.0f);
                case Size.Leaderboard:
                    return new Vector2(768.0f, 90.0f);
                case Size.Adaptive:
                    var min = 50;
                    var max = Utils.Units.ToDP(Screen.height * 0.15f);
                    return new Vector2(Utils.Units.ToDP(Screen.width), Mathf.Min(min, max));
                default:
                    return Vector2.zero;
            }
        }

        public enum Position
        {
            Top,
            Bottom,
            TopLeft,
            TopRight,
            BottomLeft,
            BottomRight,
            Center
        }

        public enum Size
        {
            /// <summary>320dp x 50dp</summary>
            Standard,
            /// <summary>320dp x 100dp</summary>
            Large,
            /// <summary>300dp x 250dp</summary>
            MediumRectangle,
            /// <summary>468dp x 60dp</summary>
            Full,
            /// <summary>728dp x 90dp</summary>
            Leaderboard,
            Adaptive,
            Smart
        }
    }
}
