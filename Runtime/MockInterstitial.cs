using System;
using UnityEngine;

namespace UMNP.GamedevKit.MobileAds
{
    public class MockInterstitial : MonoBehaviour
    {
        private Action _onClosed;

        public void SetOnClosed(Action onClosed) => _onClosed = onClosed;

        public void OnCloseButtonClicked()
        {
            _onClosed?.Invoke();
            Destroy(gameObject);
        }
    }
}
