using System;

namespace UMNP.GamedevKit.MobileAds
{
    public static class Ads
    {
        private static IMobileAdsProvider _provider;

        public static float BannerHeight => IsInitialized ? _provider.BannerHeight : 0.0f;
        public static bool IsAdsRemoved => IsInitialized ? _provider.IsAdsRemoved : false;
        public static bool IsBannerActive => IsInitialized ? _provider.IsBannerActive : false;
        public static bool IsInitialized => (_provider != null) ? _provider.IsInitialized : false;
        public static bool IsInterstitialLoaded => IsInitialized ? _provider.IsInterstitialLoaded : false;
        public static bool IsRewardedLoaded => IsInitialized ? _provider.IsRewardedLoaded : false;

        public static void CleanBanner() => _provider?.CleanBanner();
        public static void CleanInterstitial() => _provider?.CleanInterstitial();
        public static void CleanRewarded() => _provider?.CleanRewarded();

        public static void HideBanner() => _provider?.HideBanner();

        public static void Initialize(IMobileAdsProvider provider) => _provider = provider;

        public static void RemoveAds() => _provider?.RemoveAds();

        public static void RequestBanner(Banner.Size size, Banner.Position position) => _provider?.RequestBanner(size, position);
        public static void RequestInterstitial() => _provider?.RequestInterstitial();
        public static void RequestRewarded() => _provider?.RequestRewarded();

        public static void ShowBanner(Banner.Position position) => _provider?.ShowBanner(position);
        public static void ShowInterstitial(Action onClosed = null) => _provider?.ShowInterstitial(onClosed);
        public static void ShowRewarded(Action onEarned, Action onSkipped) => _provider?.ShowRewarded(onEarned, onSkipped);
    }
}
