# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [unreleased]

### Changed

- adjust SafeArea when interacting with banner, MockAds is now DontDestroyOnLoad

## [0.1.0] - 2022-04-27

### Added

- MobileAds, IMobileAdsProvider, Banner, MockAds provider

[unreleased]: https://gitlab.com/tidakjelas92/unity-gamedevkit-mobileads/-/compare/v0.1.0...main
[0.1.0]: https://gitlab.com/tidakjelas92/unity-gamedevkit-mobileads/-/tags/v0.1.0
